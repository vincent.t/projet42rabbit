﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Projet42.Services
{
    public class RabbitMQService
    {

        string queueName1 = "P42-1";
        string queueName2 = "P42-2";
        Uri RabbitMQUri = new Uri("amqp://guest:guest@localhost:5672");
        Dictionary<string, List<string>> Dictionnaire = new Dictionary<string, List<string>>();
        //requete
        public void Producer1(string Request)
        {
            var factory = new ConnectionFactory
            {
                Uri = RabbitMQUri
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(
                queueName1,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            var message = new { Name = "Producer", Message = Request };
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

           // for (int i = 0; i < 20; i++)
           // {
            //}
            channel.BasicPublish("", queueName1, null, body);
        }
        public void Consummer1()
        {
            var factory = new ConnectionFactory
            {
                Uri = RabbitMQUri
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(
                queueName1,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                if (!Dictionnaire.ContainsKey("test")) {
                        Dictionnaire.Add(
                            "test", new List<string>()
                );
            };
                Dictionnaire["test"].Add("message");
            
            };

            channel.BasicConsume(queueName1, true, consumer);

            Console.WriteLine("Consumer is listening...");
            Console.ReadLine();
        }

        //reponse
        public void Producer2(string Response)
        {
            var factory = new ConnectionFactory
            {
                Uri = RabbitMQUri
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(
                queueName2,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            var message = new { Name = "Producer", Message = Response };
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

            // for (int i = 0; i < 20; i++)
            // {
            //}
            channel.BasicPublish("", queueName2, null, body);
        }
        public void Consummer2()
        {
            var factory = new ConnectionFactory
            {
                Uri = RabbitMQUri
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(
                queueName1,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(message);
            };

            channel.BasicConsume(queueName1, true, consumer);

            Console.WriteLine("Consumer is listening...");
            Console.ReadLine();
        }


    }
}
