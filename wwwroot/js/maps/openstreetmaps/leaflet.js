﻿var outdoorsthunderforest = L.tileLayer('https://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=af071baf070341ad86b5100adeec252b', {
    maxZoom: 20
});

var outdoorsopenstreetmap = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 20
});

var hikebike = L.tileLayer('http://{s}.tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png', {
    maxZoom: 20
});
var satellite = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    maxZoom: 20
});

var map = L.map('map', {
    center: [0, 0],
    zoom: 2,
    layers: [outdoorsthunderforest],
    fullscreenControl: true
});

var baseLayers = {
    "outdoors (thunderforest.com)": outdoorsthunderforest,
    "outdoors (openstreetmap.org)": outdoorsopenstreetmap,
    "satellite": satellite,
    "hikebike": hikebike
};

L.control.layers(baseLayers).addTo(map);