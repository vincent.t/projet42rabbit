﻿function initMap() {
    let map = new GoogleMaps();
    map.initialize();

    var autocompleteOrigin = new google.maps.places.Autocomplete(document.getElementById('originAddress'));
    autocompleteOrigin.inputId = 'originAddress';

    var autocompleteDestination = new google.maps.places.Autocomplete(document.getElementById('destinationAddress'));
    autocompleteDestination.inputId = 'destinationAddress';
    
    $('#calculItineraire').click(function () {
        let origin = $("#originAddress").val();
        let destination = $("#destinationAddress").val();
        if (origin !== "" && destination !== "") {
            map.iteneraire(origin, destination);
        }
    });

    $("#modeTransport").on("click", "img", function () {
        map.changeMode($(this).attr("type"));
    });

    $("#typeEfficiency").on("click", "img", function () {
        map.changeEfficiency($(this).attr("type"));
    });
}