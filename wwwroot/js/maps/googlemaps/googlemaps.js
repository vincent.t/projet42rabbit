﻿class GoogleMaps {
    constructor() {
        this.searched = false;
        this.currentEfficiency = "duration"
        this.modeDeplacement = {
            DRIVING: {
                styles: {
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.4,
                    strokeWeight: 10
                },
                img: 'drive.png'
            },
            WALKING: {
                styles: {
                    strokeColor: '#00FF00',
                    strokeOpacity: 0.4,
                    strokeWeight: 10
                },
                img: 'walk.png'
            },
            BICYCLING: {
                styles: {
                    strokeColor: '#0000FF',
                    strokeOpacity: 0.4,
                    strokeWeight: 10
                },
                img: 'bicycle.png'
            },
            TRANSIT: {
                styles: {
                    strokeColor: '#FF9000',
                    strokeOpacity: 0.4,
                    strokeWeight: 10
                },
                img: 'transit.png'
            }
        };
        this.efficiency = {
            DURATION: {
                img: 'duration.png'
            },
            DISTANCE: {
                img: 'distance.png'
            }
        }
        this.DOMMap = {
            map: document.getElementById('map'),
            directionsPanel: document.getElementById("directionsPanel"),
            modeDeplacement: document.getElementById("modeTransport"),
            typeEfficiency: document.getElementById("typeEfficiency")
        }
    }

    initialize() {
        this.directionsService = new google.maps.DirectionsService();

        var lnt = new google.maps.LatLng(26.45, 82.85);
        var options = {
            zoom: 9,
            center: lnt,
            diagId: google.maps.MapTypeId.ROADMAP
        }
        this.map = new google.maps.Map(this.DOMMap.map, options);

        for (const [mode, _] of Object.entries(this.modeDeplacement)) {
            this.addModeTransport(mode);
        }

        for (const [type, _] of Object.entries(this.efficiency)) {
            this.addEfficiency(type);
        }
        this.setModeSelected(this.currentEfficiency, "EFFICIENCY");
    }

    iteneraire(origin, destination) {
        if (this.routes !== undefined) {
            for (const [index, _] of Object.entries(this.routes)) {
                this.routes[index].direction.setMap(null);
                this.routes[index].direction.setPanel(null);
            }
        }
        this.routes = [];
        let modeDeplacementList = Object.entries(this.modeDeplacement);
        let modeDeplacementCount = modeDeplacementList.length;
        let currentCount = 0;
        let noSpam = false;
        for (const [mode, _] of modeDeplacementList) {
            this.directionsService.route({ origin: origin, destination: destination, travelMode: google.maps.TravelMode[mode] }, (result, status) => {
                if (status == google.maps.DirectionsStatus.OK) {
                    let direction = new google.maps.DirectionsRenderer();
                    direction.setDirections(result);
                    this.routes.push({ mode: mode, direction: direction });
                } else {
                    if (!noSpam) {
                        noSpam = true;
                        alert("Cette destination est indisponible.");
                    }
                    return;
                }
            }).then(() => {
                currentCount++;
                if (currentCount === modeDeplacementCount) {
                    this.searched = true;
                    this.calculEfficiency();
                }
            });
        }
    }

    moreEfficiency() {
        return this.routes.reduce((a, b) => {
            return a.direction.directions.routes[0].legs[0][this.currentEfficiency].value < b.direction.directions.routes[0].legs[0][this.currentEfficiency].value ? a : b;
        });
    }

    calculEfficiency() {
        if (!this.searched) {
            return;
        }
        let efficient = this.moreEfficiency();
        let newStyle = this.modeDeplacement;
        newStyle[efficient.mode].styles.strokeOpacity = 1.0;
        newStyle[efficient.mode].styles.strokeWeight = 12;
        let dest = this.routes.findIndex(function (route) { return route.mode == efficient.mode });
        if (this.routes[dest] != null) {
            this.routes[dest].direction.setOptions({ polylineOptions: newStyle[efficient.mode].styles });
            this.routes[dest].direction.setPanel(this.DOMMap.directionsPanel);
            this.setModeSelected(efficient.mode);
        }
        for (const [index, route] of Object.entries(this.routes)) {
            if (index != dest) {
                this.routes[index].direction.setOptions({ polylineOptions: this.modeDeplacement[route.mode].styles });
                this.routes[index].direction.setPanel(null);

            }
            this.routes[index].direction.setMap(this.map);
        }
    }

    changeMode(mode) {
        for (const [index, route] of Object.entries(this.routes)) {
            let newStyle = this.modeDeplacement;
            newStyle[route.mode].styles.strokeOpacity = 0.4;
            newStyle[route.mode].styles.strokeWeight = 10;
            if (this.routes[index].mode == mode) {
                newStyle[mode].styles.strokeOpacity = 1.0;
                newStyle[mode].styles.strokeWeight = 12;
                this.routes[index].direction.setOptions({ polylineOptions: newStyle[mode].styles });
                this.routes[index].direction.setPanel(this.DOMMap.directionsPanel);
                this.setModeSelected(mode);
            } else {
                this.routes[index].direction.setPanel(null);
                this.routes[index].direction.setOptions({ polylineOptions: newStyle[route.mode].styles });
            }
            this.routes[index].direction.setMap(null);
            this.routes[index].direction.setMap(this.map);
        }
    }

    changeEfficiency(efficiency) {
        this.currentEfficiency = efficiency.toLowerCase();
        this.calculEfficiency();
        this.setModeSelected(efficiency, "EFFICIENCY");
    }

    addModeTransport(mode) {
        this.DOMMap.modeDeplacement.innerHTML += "<img id='mode_transport_" + mode.toLowerCase() + "' style='border: 3px " + this.modeDeplacement[mode].styles.strokeColor + " solid;' src='/imgs/deplacements/" + this.modeDeplacement[mode].img + "' type='" + mode + "' />";
    }

    addEfficiency(efficiency) {
        this.DOMMap.typeEfficiency.innerHTML += "<img id='efficiency_" + efficiency.toLowerCase() + "' src='/imgs/efficiency/" + this.efficiency[efficiency].img + "' type='" + efficiency + "' />";
    }

    setModeSelected(selectedMode, type = "TRANSPORT") {
        if (type == "TRANSPORT") {
            for (const [mode, _] of Object.entries(this.modeDeplacement)) {
                let value = this.modeDeplacement[mode];
                let dom = document.getElementById("mode_transport_" + mode.toLowerCase());
                let style = "border: 3px " + value.styles.strokeColor + " solid;";
                if (dom.getAttribute('id') == 'mode_transport_' + selectedMode.toLowerCase()) {
                    style += "background-color: " + value.styles.strokeColor + "50;";
                }
                dom.setAttribute("style", style);
            }
        } else if (type == "EFFICIENCY") {
            for (const [mode, _] of Object.entries(this.efficiency)) {
                let dom = document.getElementById("efficiency_" + mode.toLowerCase());
                let style;
                if (mode.toLowerCase() == this.currentEfficiency) {
                    style = 'border: 3px purple solid; background-color: #80008050;';
                } else {
                    style = 'border: 0; background-color: transparent;';
                }
                dom.setAttribute('style', style);
            }
        }
    }
}