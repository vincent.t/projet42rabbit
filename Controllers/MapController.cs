﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet42.Controllers
{
    [Route("api/map")]
    [ApiController]
    public class RabbitController : ControllerBase
    {
        [HttpPost("defaultMap/{map}")]
        public ActionResult<Boolean> editDefaultMap(string map)
        {
            Response.Cookies.Append("map", map);
            return true;
        }
    }
}
