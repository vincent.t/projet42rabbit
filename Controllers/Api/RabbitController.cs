﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet42.Controllers.Api
{
    [Route("api/rabbit")]
    [ApiController]
    public class RabbitController : ControllerBase
    {
        [HttpPost("request")]
        public ActionResult<Boolean> RequestMQ()
        {
            Console.WriteLine("requestMQ");
            return true;
        }

        [HttpPost("response")]
        public ActionResult<Boolean> ResponseMQ()
        {
            Console.WriteLine("responseMQ");
            return true;
        }
    }
}
