﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Projet42.Models;
using Projet42.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Projet42.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private string baseURL { get; set; }

        private void Init()
        {
            baseURL = (Request.IsHttps ? "https://" : "http://") + Request.Host;
        }

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        
        public IActionResult Index()
        {
            Init();
            ViewData["BaseURL"] = baseURL;
            ViewBag.map = Request.Cookies["map"];
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
